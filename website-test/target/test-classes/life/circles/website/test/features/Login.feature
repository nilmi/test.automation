Feature:  Login using username and password

@FunctionalTest
Scenario Outline:  Login using valid username and password

Given User already navigated to url
When User clicks on Signup button
Then Directs user to Sign in page
Then User enters valid "<email>" and "<password>" click on signin button
Then User Logs in successfully

Examples:
	| email				     	 	 | password   |
	| qatestautomation@yandex.com	 | qatest321  |