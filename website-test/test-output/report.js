$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("Login.feature");
formatter.feature({
  "line": 1,
  "name": "Login using username and password",
  "description": "",
  "id": "login-using-username-and-password",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 4,
  "name": "Login using valid username and password",
  "description": "",
  "id": "login-using-username-and-password;login-using-valid-username-and-password",
  "type": "scenario_outline",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 3,
      "name": "@FunctionalTest"
    }
  ]
});
formatter.step({
  "line": 6,
  "name": "User already navigated to url",
  "keyword": "Given "
});
formatter.step({
  "line": 7,
  "name": "User clicks on Signup button",
  "keyword": "When "
});
formatter.step({
  "line": 8,
  "name": "Directs user to Sign in page",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "User enters valid \"\u003cemail\u003e\" and \"\u003cpassword\u003e\" click on signin button",
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "User Logs in successfully",
  "keyword": "Then "
});
formatter.examples({
  "line": 12,
  "name": "",
  "description": "",
  "id": "login-using-username-and-password;login-using-valid-username-and-password;",
  "rows": [
    {
      "cells": [
        "email",
        "password"
      ],
      "line": 13,
      "id": "login-using-username-and-password;login-using-valid-username-and-password;;1"
    },
    {
      "cells": [
        "qatestautomation@yandex.com",
        "qatest321"
      ],
      "line": 14,
      "id": "login-using-username-and-password;login-using-valid-username-and-password;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.before({
  "duration": 9607017809,
  "status": "passed"
});
formatter.scenario({
  "line": 14,
  "name": "Login using valid username and password",
  "description": "",
  "id": "login-using-username-and-password;login-using-valid-username-and-password;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 3,
      "name": "@FunctionalTest"
    }
  ]
});
formatter.step({
  "line": 6,
  "name": "User already navigated to url",
  "keyword": "Given "
});
formatter.step({
  "line": 7,
  "name": "User clicks on Signup button",
  "keyword": "When "
});
formatter.step({
  "line": 8,
  "name": "Directs user to Sign in page",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "User enters valid \"qatestautomation@yandex.com\" and \"qatest321\" click on signin button",
  "matchedColumns": [
    0,
    1
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "User Logs in successfully",
  "keyword": "Then "
});
formatter.match({
  "location": "LoginStepDefinition.user_already_navigated_to_url()"
});
formatter.result({
  "duration": 106006222,
  "status": "passed"
});
formatter.match({
  "location": "LoginStepDefinition.user_clicks_on_Signup_button()"
});
formatter.result({
  "duration": 12252812068,
  "status": "passed"
});
formatter.match({
  "location": "LoginStepDefinition.directs_user_to_Sign_up_page()"
});
formatter.result({
  "duration": 9917198,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "qatestautomation@yandex.com",
      "offset": 19
    },
    {
      "val": "qatest321",
      "offset": 53
    }
  ],
  "location": "LoginStepDefinition.user_enters_valid_and_click_on_signin_button(String,String)"
});
formatter.result({
  "duration": 352899178,
  "status": "passed"
});
formatter.match({
  "location": "LoginStepDefinition.user_Logs_in_successfully()"
});
formatter.result({
  "duration": 2801284964,
  "status": "passed"
});
formatter.after({
  "duration": 112266414,
  "status": "passed"
});
});