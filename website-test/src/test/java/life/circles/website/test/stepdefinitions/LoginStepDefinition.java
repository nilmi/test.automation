package life.circles.website.test.stepdefinitions;

import org.junit.Assert;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import life.circles.website.test.pages.HomePage;
import life.circles.website.test.pages.MyAccountPage;
import life.circles.website.test.pages.PlanPage;
import life.circles.website.test.pages.SigninPage;
import life.circles.website.test.testbase.TestBase;

public class LoginStepDefinition extends TestBase {

	HomePage homePage;
	SigninPage signinPage;
	PlanPage planPage;
	MyAccountPage myAccountPage;

	/**
	 * runs before each test
	 */
	@Before
	public void setup() {
		TestBase.init();
		homePage = new HomePage();
	}

	/**
	 * perform basic initialization task
	 */
	public LoginStepDefinition() {
		super();
	}

	@Given("^User already navigated to url$")
	public void user_already_navigated_to_url() {

		// Get the title of the home page from HomePage validateHomePageTitle method
		String title = homePage.getHomePageTitle();

		// Assert the title with expected title
		// Read the expected value from configuration file
		String mainPageTitle = prop.getProperty("home.page.title");
		Assert.assertEquals(mainPageTitle, title);
		signinPage = new SigninPage();
	}

	@When("^User clicks on Signup button$")
	public void user_clicks_on_Signup_button() throws InterruptedException {
		homePage.clickSignupButton();
		closeFacebookPopup();
	}

	@Then("^Directs user to Sign in page$")
	public void directs_user_to_Sign_up_page() {
		// Check the sign in page title
		Assert.assertEquals(prop.getProperty("signin.page.title"), signinPage.getSigninPageTitle());
	}

	@Then("^User enters valid \"([^\"]*)\" and \"([^\"]*)\" click on signin button$")
	public void user_enters_valid_and_click_on_signin_button(String username, String password) {
		// Enter valid username and password and login
		// CommonStepDefinitions.signinPage.clickOnSigninTab();
		planPage = signinPage.signin(username, password);
	}

	@Then("^User Logs in successfully$")
	public void user_Logs_in_successfully() {
		/*
		 * Go to my account page from plan page and check whether correct email address
		 * displayed in my account page
		 */
		myAccountPage = planPage.clickMyAccountLink();
		Assert.assertEquals(prop.getProperty("email"), myAccountPage.getLoginEmail());
	}

	/**
	 * runs after each test
	 */
	@After
	public void cleanup() {
		super.cleanup();
	}
}
