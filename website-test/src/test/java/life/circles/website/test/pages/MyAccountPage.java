package life.circles.website.test.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import life.circles.website.test.testbase.TestBase;

public class MyAccountPage extends TestBase {

	// Page Factory / Object Repository of SigninPage

	@FindBy(how = How.XPATH, using = "//label[text()='Email']//following-sibling::div//input[@type='text']")
	WebElement myAccountLink;

	/**
	 * Initialize the MyAccountPage page factory using PageFactory class initElement
	 * method
	 */
	public MyAccountPage() {
		PageFactory.initElements(driver, this);
	}

	public String getLoginEmail() {
		return myAccountLink.getAttribute("value");
	}

}
