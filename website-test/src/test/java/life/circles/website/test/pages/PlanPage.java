package life.circles.website.test.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import life.circles.website.test.testbase.TestBase;

public class PlanPage extends TestBase {

	// Page Factory / Object Repository of SigninPage

	@FindBy(how = How.XPATH, using = "//div[text()='MY ACCOUNT']")
	WebElement myAccountLink;

	@FindBy(how = How.XPATH, using = "//a[@href='https://www.facebook.com/CirclesLifeSG/']")
	WebElement facebookLink;

	/**
	 * Initialize the PlanPage page factory using PageFactory class initElement
	 * method
	 */
	public PlanPage() {
		PageFactory.initElements(driver, this);
	}

	/**
	 * Click on 'My Account' link
	 * 
	 * @return new MyAccountPage object
	 */
	public MyAccountPage clickMyAccountLink() {
		new WebDriverWait(driver, 20).until(ExpectedConditions.visibilityOf(myAccountLink));
		myAccountLink.click();
		return new MyAccountPage();
	}

}
