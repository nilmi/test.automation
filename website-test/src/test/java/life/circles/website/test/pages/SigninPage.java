package life.circles.website.test.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import life.circles.website.test.testbase.TestBase;

public class SigninPage extends TestBase {

	// Page Factory / Object Repository of SigninPage

	@FindBy(how = How.XPATH, using = "//span[text()='SIGN IN']")
	WebElement signinTab;

	@FindBy(how = How.XPATH, using = "//input[@name='email' and @type='text']")
	WebElement email;

	@FindBy(how = How.XPATH, using = "//input[@name='password' and @type='password']")
	WebElement password;

	@FindBy(how = How.XPATH, using = "//button[text()='Sign In']")
	WebElement signinButton;

	@FindBy(how = How.XPATH, using = "//button[@class='btn btn-primary btn-lg btn-block facebook-login-button metro']")
	WebElement loginWithFbButton;

	/**
	 * Initialize the SigninPage page factory using PageFactory class initElement
	 * method
	 */
	public SigninPage() {
		PageFactory.initElements(driver, this);
	}

	/**
	 * Enter email address. password and sign-in
	 * 
	 * @return new PlanPage object
	 */
	public PlanPage signin(String userEmail, String userpassword) {
		email.sendKeys(userEmail);
		password.sendKeys(userpassword);
		signinButton.click();
		return new PlanPage();
	}

	/**
	 * Returns page title
	 * @return page title
	 */
	public String getSigninPageTitle() {
		return driver.getTitle();
	}

}
