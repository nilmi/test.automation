package life.circles.website.test.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import life.circles.website.test.testbase.TestBase;

public class HomePage extends TestBase {

	// Page Factory / Object Repository of HomePage

	@FindBy(how = How.XPATH, using = "//nav[@id='site-navigation']//div//div[@class='dn db-l']//a[text()='Sign up']")
	WebElement signupButton;

	/**
	 * Initialize the HomePage page factory using PageFactory class initElement
	 * method
	 */
	public HomePage() {
		PageFactory.initElements(driver, this);
	}

	/**
	 * click on sign up link
	 * 
	 * @return sign in page object
	 */
	public SigninPage clickSignupButton() {
		signupButton.click();
		return new SigninPage();
	}

	/**
	 * get home page title
	 * 
	 * @return home page title
	 */
	public String getHomePageTitle() {
		return driver.getTitle();
	}

}
