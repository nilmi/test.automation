package com.facebook.test.testbase;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;

public class TestBase {

	public static Properties prop;
	public static WebDriver driver;

	/**
	 * Perform basic initialization task read properties file
	 */
	public TestBase() {
		try {
			prop = new Properties();
			FileInputStream inputStream = new FileInputStream(
					"src/test/resources/com/facebook/" + "test/config/config.properties");
			prop.load(inputStream);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Initialize app
	 * 
	 * @throws MalformedURLException
	 */
	public static void init() throws MalformedURLException {
		// Setup desired capabilities, pass appActivity and appPackage to appium
		DesiredCapabilities capabilities = new DesiredCapabilities();

		capabilities.setCapability("platformName", "android");
		capabilities.setCapability("deviceName", "nexus_5x_p");
		capabilities.setCapability("appActivity", "com.facebook.katana.LoginActivity");
		capabilities.setCapability("appPackage", "com.facebook.katana");
		capabilities.setCapability("noReset", false);
		capabilities.setCapability(AndroidMobileCapabilityType.AUTO_GRANT_PERMISSIONS, true);

		// Instantiate Appium Driver
		driver = new AndroidDriver(new URL("http://0.0.0.0:4723/wd/hub"), capabilities);

	}

	/**
	 * Close app
	 */
	public void cleanup() {
		if (TestBase.driver != null) {
			try {
				TestBase.driver.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

}
