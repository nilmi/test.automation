package com.facebook.test.page;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.facebook.test.testbase.TestBase;

public class LoginPage extends TestBase {

	// Page Factory / Object Repository of HomePage

	@FindBy(how = How.XPATH, using = "//android.widget.EditText[@text='Phone or Email']")
	WebElement emailField;

	@FindBy(how = How.XPATH, using = "//android.widget.EditText[@text='Password']")
	WebElement passwordField;

	@FindBy(how = How.XPATH, using = "//android.widget.Button[@text='LOG IN']")
	WebElement loginButton;

	/**
	 * Initialize the HomePage page factory using PageFactory class initElement
	 * method
	 */
	public LoginPage() {
		PageFactory.initElements(driver, this);
	}

	/**
	 * Enter email and password and login
	 * 
	 * @return new HomePage object
	 */
	public RememberPasswordPage login(String email, String password) {
		emailField.sendKeys(email);
		passwordField.sendKeys(password);
		loginButton.click();
		return new RememberPasswordPage();
	}
}
