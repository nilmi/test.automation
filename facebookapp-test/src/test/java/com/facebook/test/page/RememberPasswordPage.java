package com.facebook.test.page;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.facebook.test.testbase.TestBase;

public class RememberPasswordPage extends TestBase {
	// Page Factory / Object Repository of HomePage

	@FindBy(how = How.XPATH, using = "//android.widget.Button[@text='Not Now']")
	WebElement notNowButton;

	/**
	 * Initialize the RememberPasswordPage page factory using PageFactory class
	 * initElement method
	 */
	public RememberPasswordPage() {
		PageFactory.initElements(driver, this);
	}

	/**
	 * Click on not now button of remember password page
	 * 
	 * @return new HomePage object
	 */
	public HomePage clickNotNowButton() {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.elementToBeClickable(notNowButton));
		notNowButton.click();
		return new HomePage();
	}
}
