package com.facebook.test.page;

import org.openqa.selenium.By;

import com.facebook.test.testbase.TestBase;

public class PostPage extends TestBase {

	/**
	 * Returns true if comment is displayed
	 * 
	 * @param commentText
	 * @return true if comment is displayed
	 */
	public boolean checkComment(String commentText) {
		return driver.findElement(By.xpath("//android.view.ViewGroup[@text='" + commentText + "']")).isDisplayed();
	}

}
