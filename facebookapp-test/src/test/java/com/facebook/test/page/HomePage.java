package com.facebook.test.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.facebook.test.testbase.TestBase;

public class HomePage extends TestBase {
	// Page Factory / Object Repository of HomePage

	@FindBy(how = How.XPATH, using = "//android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout[4]")
	WebElement drawerMenuButton;

	@FindBy(how = How.XPATH, using = "//android.widget.EditText[@text='Search']")
	WebElement searchBar;

	/**
	 * Initialize the RememberPasswordPage page factory using PageFactory class
	 * initElement method
	 */
	public HomePage() {
		PageFactory.initElements(driver, this);
	}

	/**
	 * Click on drawer menu
	 */
	public void clickDrawerMenuButton() {
		drawerMenuButton.click();
	}

	/**
	 * Search profile by profile name
	 * 
	 * @param profileName
	 *            profile name
	 */
	public void searchProfile(String profileName) {
		searchBar.sendKeys(profileName);
		driver.findElement(By.xpath("//android.widget.EditText[@text='" + profileName + "']")).submit();
		driver.findElement(By.xpath("//android.widget.ImageView[@content-desc='" + profileName.toLowerCase() + "']"))
				.click();
	}

	/**
	 * Click on commented post
	 * 
	 * @return new post page object
	 */
	public PostPage clickOnPost() {
		driver.findElement(By.xpath("//android.view.ViewGroup[@text='Qatest's cover photo']")).click();
		return new PostPage();
	}

}
