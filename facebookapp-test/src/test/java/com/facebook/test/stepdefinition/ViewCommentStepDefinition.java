package com.facebook.test.stepdefinition;

import java.net.MalformedURLException;

import com.facebook.test.page.HomePage;
import com.facebook.test.page.LoginPage;
import com.facebook.test.page.PostPage;
import com.facebook.test.page.RememberPasswordPage;
import com.facebook.test.testbase.TestBase;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.When;
import junit.framework.Assert;

public class ViewCommentStepDefinition extends TestBase {

	LoginPage loginPage;
	HomePage homePage;
	RememberPasswordPage rememberPasswordPage;
	PostPage postPage;

	/**
	 * runs before each test
	 * 
	 * @throws MalformedURLException
	 */
	@Before
	public void setup() throws MalformedURLException {
		TestBase.init();
		loginPage = new LoginPage();
	}

	/**
	 * perform basic initialization task
	 */
	public ViewCommentStepDefinition() {
		super();
	}

	// Step definitions

	@When("^User enters valid \"([^\"]*)\" and \"([^\"]*)\" click on login button$")
	public void user_enters_valid_and_click_on_login_button(String email, String password) {
		rememberPasswordPage = loginPage.login(email, password);
		homePage = rememberPasswordPage.clickNotNowButton();
	}

	@When("^User selects post$")
	public void user_navigates_to_profile() {
		homePage.searchProfile(prop.getProperty("name.profile"));
		postPage = homePage.clickOnPost();
	}

	@When("^User views the post$")
	public void user_views_the_post() {
		Assert.assertTrue(postPage.checkComment(prop.getProperty("post")));
	}

	/**
	 * runs after each test
	 */
	@After
	public void cleanup() {
		super.cleanup();
	}

}
