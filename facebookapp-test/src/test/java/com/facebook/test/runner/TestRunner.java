package com.facebook.test.runner;

import java.io.File;

import org.junit.AfterClass;
import org.junit.runner.RunWith;

import com.cucumber.listener.Reporter;
import com.facebook.test.testbase.TestBase;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/com/facebook/test/feature/", glue = {
		"com.facebook.test.stepdefinition" }, dryRun = false, format = { "pretty", "html:test-output",
				"json:json-output/cucumber.json",
				"junit:junit-xml/cucumber.xml" }, monochrome = true, strict = true, tags = {
						"@FunctionalTest" }, plugin = {
								"com.cucumber.listener.ExtentCucumberFormatter:target/cucumber-reports/report.html" })
public class TestRunner extends TestBase {
	@AfterClass
	public static void writeExtentReport() {
		Reporter.loadXMLConfig(new File(prop.getProperty("report.path")));
	}
}
