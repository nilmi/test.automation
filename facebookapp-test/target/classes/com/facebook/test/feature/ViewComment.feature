Feature:  Login to Facebook and post a comment

@FunctionalTest
Scenario Outline:  Login to Facebook using valid username and password and post a comment

When User enters valid "<email>" and "<password>" click on login button
Then User selects post
Then User views the post


Examples:
	| email				     	 	 | password   	  	|
	| qatestautomation@yandex.com	 | qatest321  	|