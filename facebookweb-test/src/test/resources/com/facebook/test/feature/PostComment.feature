Feature:  Login to Facebook and post a comment

@FunctionalTest
Scenario Outline:  Login to Facebook using valid username and password and post a comment

Given User already navigated to Facebook url
Then User enters valid "<email>" and "<password>" click on login button
Then Directs user to home page
Then User navigates to profile page
Then User posts a comment

Examples:
	| email				     	 	 | password   	  	|
	| qatestautomation@yandex.com	 | qatest321 	|