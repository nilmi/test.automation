package com.facebook.test.page;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.facebook.test.testbase.TestBase;

public class LoginPage extends TestBase {

	// Page Factory / Object Repository of HomePage

	@FindBy(how = How.ID, using = "email")
	WebElement emailField;

	@FindBy(how = How.ID, using = "pass")
	WebElement passwordField;

	@FindBy(how = How.ID, using = "loginbutton")
	WebElement loginButton;

	/**
	 * Initialize the HomePage page factory using PageFactory class initElement
	 * method
	 */
	public LoginPage() {
		PageFactory.initElements(driver, this);
	}

	/**
	 * Return page title
	 * 
	 * @return
	 */
	public String getUrl() {
		return driver.getCurrentUrl();
	}

	/**
	 * Signin to Facebook
	 * 
	 * @param email
	 *            email address
	 * @param password
	 *            password
	 * @return new Homepage object
	 */
	public HomePage signIn(String email, String password) {
		emailField.sendKeys(email);
		passwordField.sendKeys(password);
		loginButton.click();
		return new HomePage();
	}

}
