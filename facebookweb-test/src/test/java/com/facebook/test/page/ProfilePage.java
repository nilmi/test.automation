package com.facebook.test.page;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.facebook.test.testbase.TestBase;

public class ProfilePage extends TestBase {

	// Page Factory / Object Repository of HomePage

	@FindBy(how = How.XPATH, using = "//*[@id='addComment_104573193744140']/div/div[2]/div/div/div/div[1]/div")
	WebElement latestPostCommentField;

	/**
	 * Initialize the HomePage page factory using PageFactory class initElement
	 * method
	 */
	public ProfilePage() {
		PageFactory.initElements(driver, this);
	}

	/**
	 * Posts a comment
	 * 
	 * @param comment
	 *            comment to be added
	 * @throws InterruptedException
	 */
	public void postComment(String comment) throws InterruptedException {
		// Waits until latestPostCommentField is clickable
		WebDriverWait wait = new WebDriverWait(driver, 30);

		// Clicks on comment fields
		latestPostCommentField.click();
		wait.until(ExpectedConditions.elementToBeClickable(latestPostCommentField));

		// Post comment
		Actions action = new Actions(driver);
		action.sendKeys(latestPostCommentField, comment + Keys.RETURN).build().perform();

	}

}
