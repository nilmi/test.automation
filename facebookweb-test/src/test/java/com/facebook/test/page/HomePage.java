package com.facebook.test.page;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.facebook.test.testbase.TestBase;

public class HomePage extends TestBase {

	// Page Factory / Object Repository of HomePage

	@FindBy(how = How.XPATH, using = "//img[contains(@id,'profile_pic_header_')]")
	WebElement profileIcon;

	/**
	 * Initialize the HomePage page factory using PageFactory class initElement
	 * method
	 */
	public HomePage() {
		PageFactory.initElements(driver, this);
	}

	/**
	 * Click on profile icon located in header
	 * 
	 * @return new ProfilePage object
	 */
	public ProfilePage clickProfileIcon() {
		// Waits until profile icon to be clickable
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.elementToBeClickable(profileIcon));

		/*
		 * Element not getting clicked due to JavaScript or AJAX calls present Used
		 * Actions class due to that reason
		 */
		Actions action = new Actions(driver);
		action.click(profileIcon).build().perform();
		return new ProfilePage();
	}

	/**
	 * Returns page title
	 * 
	 * @return page title
	 */
	public String getTitle() {
		return driver.getTitle();
	}

}
