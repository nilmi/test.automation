package com.facebook.test.stepdefinitions;

import com.facebook.test.page.HomePage;
import com.facebook.test.page.LoginPage;
import com.facebook.test.page.ProfilePage;
import com.facebook.test.testbase.TestBase;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import junit.framework.Assert;

public class PostCommentStepDefinition extends TestBase {

	LoginPage loginPage;
	HomePage homePage;
	ProfilePage profilePage;

	/**
	 * runs before each test
	 */
	@Before
	public void setup() {
		TestBase.init();
		loginPage = new LoginPage();
	}

	/**
	 * perform basic initialization task
	 */
	public PostCommentStepDefinition() {
		super();
	}

	@Given("^User already navigated to Facebook url$")
	public void user_already_navigated_to_Facebook_url() {
		Assert.assertEquals(prop.getProperty("url"), loginPage.getUrl());
	}

	@Then("^User enters valid \"([^\"]*)\" and \"([^\"]*)\" click on login button$")
	public void user_enters_valid_and_click_on_login_button(String email, String password) {
		homePage = loginPage.signIn(email, password);
	}

	@Then("^Directs user to home page$")
	public void directs_user_to_home_page() {
		Assert.assertEquals(prop.getProperty("title.home.page"), homePage.getTitle());
	}

	@Then("^User navigates to profile page$")
	public void user_navigates_to_profile_page() {
		profilePage = homePage.clickProfileIcon();
	}

	@Then("^User posts a comment$")
	public void user_adds_a_comment() throws InterruptedException {
		profilePage.postComment("Test comment");
	}

	/**
	 * runs after each test
	 */
	@After
	public void cleanup() {
		super.cleanup();
	}

}
