$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("PostComment.feature");
formatter.feature({
  "line": 1,
  "name": "Login to Facebook and post a comment",
  "description": "",
  "id": "login-to-facebook-and-post-a-comment",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 4,
  "name": "Login to Facebook using valid username and password and post a comment",
  "description": "",
  "id": "login-to-facebook-and-post-a-comment;login-to-facebook-using-valid-username-and-password-and-post-a-comment",
  "type": "scenario_outline",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 3,
      "name": "@FunctionalTest"
    }
  ]
});
formatter.step({
  "line": 6,
  "name": "User already navigated to Facebook url",
  "keyword": "Given "
});
formatter.step({
  "line": 7,
  "name": "User enters valid \"\u003cemail\u003e\" and \"\u003cpassword\u003e\" click on login button",
  "keyword": "Then "
});
formatter.step({
  "line": 8,
  "name": "Directs user to home page",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "User navigates to profile page",
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "User posts a comment",
  "keyword": "Then "
});
formatter.examples({
  "line": 12,
  "name": "",
  "description": "",
  "id": "login-to-facebook-and-post-a-comment;login-to-facebook-using-valid-username-and-password-and-post-a-comment;",
  "rows": [
    {
      "cells": [
        "email",
        "password"
      ],
      "line": 13,
      "id": "login-to-facebook-and-post-a-comment;login-to-facebook-using-valid-username-and-password-and-post-a-comment;;1"
    },
    {
      "cells": [
        "qatestautomation@yandex.com",
        "qatest321"
      ],
      "line": 14,
      "id": "login-to-facebook-and-post-a-comment;login-to-facebook-using-valid-username-and-password-and-post-a-comment;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.before({
  "duration": 3464606005,
  "status": "passed"
});
formatter.scenario({
  "line": 14,
  "name": "Login to Facebook using valid username and password and post a comment",
  "description": "",
  "id": "login-to-facebook-and-post-a-comment;login-to-facebook-using-valid-username-and-password-and-post-a-comment;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 3,
      "name": "@FunctionalTest"
    }
  ]
});
formatter.step({
  "line": 6,
  "name": "User already navigated to Facebook url",
  "keyword": "Given "
});
formatter.step({
  "line": 7,
  "name": "User enters valid \"qatestautomation@yandex.com\" and \"qatest321\" click on login button",
  "matchedColumns": [
    0,
    1
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 8,
  "name": "Directs user to home page",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "User navigates to profile page",
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "User posts a comment",
  "keyword": "Then "
});
formatter.match({
  "location": "PostCommentStepDefinition.user_already_navigated_to_Facebook_url()"
});
formatter.result({
  "duration": 121230480,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "qatestautomation@yandex.com",
      "offset": 19
    },
    {
      "val": "qatest321",
      "offset": 53
    }
  ],
  "location": "PostCommentStepDefinition.user_enters_valid_and_click_on_login_button(String,String)"
});
formatter.result({
  "duration": 3558799429,
  "status": "passed"
});
formatter.match({
  "location": "PostCommentStepDefinition.directs_user_to_home_page()"
});
formatter.result({
  "duration": 7621741,
  "status": "passed"
});
formatter.match({
  "location": "PostCommentStepDefinition.user_navigates_to_profile_page()"
});
formatter.result({
  "duration": 1043773525,
  "status": "passed"
});
formatter.match({
  "location": "PostCommentStepDefinition.user_adds_a_comment()"
});
formatter.result({
  "duration": 4218849572,
  "status": "passed"
});
formatter.after({
  "duration": 123880235,
  "status": "passed"
});
});