**website test project:**

Project Details:

- Programming Language: Java
- Framework: Page Object Model using Selenium and Cucumber
- IDE Used: Eclipse
- External Libraries: Selenium, Cucumber, JUnit, extent reports
- (External libraries are defined in the maven pom.xml file)
- Project name: website-test
- Bitbucket repository path: https://bitbucket.org/nilmi/circles.life/src/master/website-test/

Features:

- Test code is runnable and tested on Windows, Mac OS X and Linux.
- Automation scripts automates the functionality in UI within a browser.
- Reports will be generated at the end to the target/cucumber-reports directory (Extent reports), test-output directory, json\_output directory and junit-xml directory in the project.
- The host can be configured via the configuration file config.properties.
- Test can be configured to run on various browsers as configured using the file config.properties.
- Test service credentials and all other settings can be configured using the file config.properties.
- Headless browser functionality has been implemented ChromeHeadless browser.
- Screenshots will be captured for test failures using WebDriverEventListener.
- Project is based on maven.
- Git is used for source code management.

**Facebook website test project:**

Project Details:

- Programming Language: Java
- Framework: Page Object Model using Selenium and Cucumber
- IDE Used: Eclipse
- External Libraries: Selenium, Cucumber, JUnit, extent reports
- (External libraries are defined in the maven pom.xml file)
- Project name: facebookweb-test
- Bitbucket repository path: https://bitbucket.org/nilmi/circles.life/src/master/facebookweb-test/

Features:

- Test code is runnable and tested on Windows, Mac OS X and Linux.
- Automation scripts automates the functionality in UI within a browser.
- Reports will be generated at the end to the target/cucumber-reports directory (Extent reports), test-output directory, json\_output directory and junit-xml directory in the project.
- The host can be configured via the configuration file config.properties.
- Test can be configured to run on various browsers as configured using the file config.properties.
- Test service credentials and all other settings can be configured using the file config.properties.
- Headless browser functionality has been implemented ChromeHeadless browser
- Screenshots will be captured for test failures using WebDriverEventListener.
- Project is based on maven.
- Git is used for source code management.

**Facebook app test project:**

Project Details:

- Programming Language: Java
- Framework: Page Object Model using Selenium, Appium and Cucumber
- IDE Used: Eclipse
- External Libraries: Selenium, Cucumber, Appium, JUnit, extent reports
- (External libraries are defined in the maven pom.xml file)
- Project name: facebookapp-test
- Bitbucket repository path: https://bitbucket.org/nilmi/circles.life/src/master/facebookapp-test/

Features:

- Automation scripts automates the functionality in UI within a browser.
- Reports will be generated at the end to the target/cucumber-reports directory (Extent reports), test-output directory, json\_output directory and junit-xml directory in the project.
- Test service credentials and all other settings can be configured using the file config.properties.
- Project is based on maven.
- Git is used for source code management.